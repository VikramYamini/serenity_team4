package stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Pageobjectmodel {

    WebDriver driver;

    By txt_Username = By.xpath("//input[@type='number']"),
            txt_Password = By.xpath("//input[@type='password']"),
            txt_captcha = By.xpath("//div[@class='recaptcha-checkbox-border']"),
            login_button = By.xpath("//button[@class='login-button']"),
            logout_button = By.xpath("//button[@title='KYC Pending']");

    public Pageobjectmodel(WebDriver driver) {
        this.driver = driver;
    }

    public void enterusername(String username){
        driver.findElement(txt_Username).sendKeys(username);
    }
    public void enterpassword(String password){
        driver.findElement(txt_Password).sendKeys(password);
    }
    public void validatecaptcha(){
        driver.findElement(txt_captcha).click();

    }
    public void clickonlogin(){
        driver.findElement(login_button).click();

    }
    public void clickonlogout(){
        driver.findElement(logout_button);
    }


}
