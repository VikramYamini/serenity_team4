package stepdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Steps {
    WebDriver driver ;

    Pageobjectmodel login=new Pageobjectmodel(driver);

    @Given("chrome browser is open and in login page")
    public void chrome_browser_is_open_and_in_login_page()  {
        System.setProperty("webdriver.chrome.driver","D:\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://dash.testgritfirst.com/");
        System.out.println(driver.getTitle());
        driver.manage().window().maximize();
        //driver.manage().timeouts.
    }
    @When("^user enters (.*) and (.*)$")
    public void user_enters_and(String username, String password) {

        login.enterusername(username);
        login.enterpassword(password);

    }

    @When("user click on login button")
    public void user_click_on_login_button() {
        login.validatecaptcha();

    }

    @Then("user navigates to dashboard and logout")
    public void user_navigates_to_dashboard_and_logout() {
        login.clickonlogin();
        login.clickonlogout();


    }

}
