Feature: user login

  Scenario Outline:
  Login to application
    Given chrome browser is open and in login page
    When user enters <username> and <password>
    When user click on login button
    Then user navigates to dashboard and logout
    Examples:
      | username | password |
      | "2862887335" | "Grit@987@" |